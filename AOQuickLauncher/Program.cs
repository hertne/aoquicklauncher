﻿using Bleak;
using System;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace AOQuickLauncher
{
    public class Program
    {
        private static void Main(string[] args)
        {
            if(args.Length < 3)
            {
                Console.WriteLine("Invalid Args");
                Environment.Exit(0);
            }

            Console.WriteLine(Environment.GetEnvironmentVariable("AOPath"));
            Process client = SpawnAOProcess(args[0], args[1], args[2], args.Last() == "/quiet");
            Console.WriteLine($"{Convert.ToString(false)}");
            using Injector injector = new Injector(client.Id, $"{AppContext.BaseDirectory}\\LoginHandler.dll", InjectionMethod.CreateThread, InjectionFlags.None);
            injector.InjectDll();
        }

        private static Process SpawnAOProcess(string username, string password, string charId, bool quietMode)
        {
            string? exportedPath = Environment.GetEnvironmentVariable("AOPath");

            string aoPath = exportedPath != null ? exportedPath : AppContext.BaseDirectory;

            Console.WriteLine(aoPath);
            Process client = new Process();
            client.StartInfo.WorkingDirectory = aoPath;
            client.StartInfo.FileName = $"{aoPath}\\AnarchyOnline.exe";
            client.StartInfo.Arguments = "IA700453413 IP7505 DU";
            client.StartInfo.EnvironmentVariables.Add("AOUser", username);
            client.StartInfo.EnvironmentVariables.Add("AOPass", password);
            client.StartInfo.EnvironmentVariables.Add("AOCharId", charId);
            client.StartInfo.EnvironmentVariables.Add("QuietLaunch", Convert.ToString(quietMode));
            client.Start();
            return client;
        }
    }
}
